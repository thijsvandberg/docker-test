# Docker Compose usage
```jaml
version: '2'
services:
  web:
    image: sebwite-docker
    ports:
      - "127.0.0.3:8080:80"
    volumes:
      - /Users/Thijs/docker/sebwite-compass/src:/var/www/html
  compass:
    image: thijsvandberg/sebwite-compass
    command: watch --poll /src
    volumes:
      - /Users/Thijs/docker/sebwite-compass/src/wp-content/themes/zermelo/assets:/src
```